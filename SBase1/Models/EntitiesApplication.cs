﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SBase1.Models
{
    public class EntitiesApplication : DbContext
    {
        public EntitiesApplication() : base("name=EntitiesConnectString") { }

        public DbSet<FirstTable> FirstTables { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // khai báo Id sẽ là khóa chính
            modelBuilder.Entity<FirstTable>().HasKey(b => b.Id);

            // khai báo Id sẽ tự động tăng
            modelBuilder.Entity<FirstTable>().Property(b => b.Id)
            .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            base.OnModelCreating(modelBuilder);
        }

    }

    public class FirstTable
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}