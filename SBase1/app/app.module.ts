import { NgModule, enableProdMode } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

/**
 * Custom Module
 */
import { routing } from './app.routing';
import { httpClient } from './http-client.component';

/**
 * Components
 */

import { AppComponent } from './Components/app.component';
import { HomeComponent } from './Components/home/home.component';
import { AboutComponent } from './Components/about/about.component';
import { UserComponent } from './Components/user/user.component';
import { LoginComponent } from './Components/login/login.component';


/**
 * Services
 */
import { MainService } from './Services/main.service';

enableProdMode();
@NgModule({
  declarations: [
      AppComponent,
      HomeComponent,
      AboutComponent,
      UserComponent,
      LoginComponent
  ],
  imports: [
      BrowserModule, ReactiveFormsModule, HttpModule, routing
    ],
    providers: [{ provide: APP_BASE_HREF, useValue: '/' },
        MainService
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
