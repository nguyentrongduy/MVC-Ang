﻿import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { RouterModule, Router } from '@angular/router';

import { IUser } from '../../Models/user';
import { MainService } from '../../Services/main.service';
import { ApiUrl } from '../../Common/globalUtil';


@Component({
    selector: 'user',
    templateUrl: 'app/Components/user/user.component.html'
})

export class UserComponent {
    users: IUser[];
    user: IUser;
    msg: string;
    indLoading: boolean = false;
    stogToken: string;

    constructor(private fb: FormBuilder,
        private _mainService: MainService,
        private router: Router) { }

    onGet(): void {
        this._mainService.get(ApiUrl.getUserInfo)
            .subscribe(users => {
                this.users = users; this.indLoading = false; console.log(users)
            },
            error => this.msg = <any>error);
    }

    onPost(): void {
        this._mainService.post(ApiUrl.checkLogin, null)
            .subscribe(response => {
                this.users = response; this.indLoading = false; console.log(response)
                localStorage.setItem('UToken', response.token);
                this.stogToken = localStorage.getItem('UToken');
                console.log(this.stogToken)
            },
            error => this.msg = <any>error);
    }

    onPut() {
        console.log(3)
    }

    onDelete() {
        console.log(4)
    }
}