﻿import { Component } from '@angular/core';
@Component({
    selector: 'about',
    templateUrl: 'app/Components/about/about.component.html'
})
export class AboutComponent {
    text = 'About Component';

    onSubmit = function (formData) {
        this._membersService.login(formData).subscribe((response) => {
            localStorage.setItem('UToken', 'K4L7V7z2beNhmijwiHw98bwO1MvofEiT');
        },
            (err) => console.log(err)
        );
    }
} 