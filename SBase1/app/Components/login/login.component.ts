﻿import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';

import { IUser } from '../../Models/user';
import { MainService } from '../../Services/main.service';
import { ApiUrl } from '../../Common/globalUtil';


@Component({
    selector: 'login',
    templateUrl: 'app/Components/login/login.component.html'
})

export class LoginComponent implements OnInit, OnDestroy {
    ngOnDestroy() {
        this.sub.unsubscribe();
    }
    msg: string;
    indLoading: boolean = false;
    backUrl: string;
    private sub: any;
    res: any;

    constructor(private fb: FormBuilder,
        private _mainService: MainService,
        private _router: Router,
        private route: ActivatedRoute) { }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            this.backUrl = params['backUrl'];
        });
        this._mainService.checkLogin(ApiUrl.checkLogin)
            .subscribe(res => {
                this.res = res; this.indLoading = false;
                if (!res) {
                    this._router.navigate([this.backUrl])
                }
            },
            error => this.msg = <any>error);
    }
}