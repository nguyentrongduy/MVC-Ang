﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './Components/home/home.component';
import { AboutComponent } from './Components/about/about.component';
import { UserComponent } from './Components/user/user.component';
import { LoginComponent } from './Components/login/login.component';


const appRoutes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'about', component: AboutComponent },
    { path: 'user', component: UserComponent },
    { path: 'login/:backUrl', component: LoginComponent },
    { path: 'login', component: LoginComponent },

    { path: '**', redirectTo: 'home', pathMatch: 'full'}
];

export const routing: ModuleWithProviders =
    RouterModule.forRoot(appRoutes);