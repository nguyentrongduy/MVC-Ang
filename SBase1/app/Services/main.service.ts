﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from "@angular/router";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';

import { httpClient } from '../http-client.component';

@Injectable()
export class MainService {
    constructor(private _http: Http, private _router: Router) { }

    ///# Xử lý request ===============================================================================================
    get(url: string): Observable<any> {
        return this._http.get(url, httpClient)
            .map((response: Response) => {
                if (response) {
                    if (response.status === 201) {
                        return <any>response.json();
                    }
                    else if (response.status === 200) {
                        return <any>response.json();
                    }
                }
            })
            .catch((error: any) => {
                if (error.status === 401) {
                    this.handleUnauthorized();
                }
                else {
                    return this.throwError(error);
                }
            });
    }

    post(url: string, model: any): Observable<any> {
        let body = JSON.stringify(model);
        return this._http.post(url, body, httpClient)
            .map((response: Response) => {
                if (response) {
                    if (response.status === 201) {
                        return <any>response.json();
                    }
                    else if (response.status === 200) {
                        return <any>response.json();
                    }
                }
            })
            .catch((error: any) => {
                if (error.status === 401) {
                    this.handleUnauthorized();
                }
                else {
                    return this.throwError(error);
                }
            });
    }

    put(url: string, id: number, model: any): Observable<any> {
        let body = JSON.stringify(model);
        return this._http.put(url + id, body, httpClient)
            .map((response: Response) => {
                if (response) {
                    if (response.status === 201) {
                        return <any>response.json();
                    }
                    else if (response.status === 200) {
                        return <any>response.json();
                    }
                }
            })
            .catch((error: any) => {
                if (error.status === 401) {
                    this.handleUnauthorized();
                }
                else {
                    return this.throwError(error);
                }
            });
    }

    delete(url: string, id: number): Observable<any> {
        return this._http.delete(url + id, httpClient)
            .map((response: Response) => {
                if (response) {
                    if (response.status === 201) {
                        return <any>response.json();
                    }
                    else if (response.status === 200) {
                        return <any>response.json();
                    }
                }
            })
            .catch((error: any) => {
                if (error.status === 401) {
                    this.handleUnauthorized();
                }
                else {
                    return this.throwError(error);
                }
            });
    }

    checkLogin(url: string): Observable<any> {
        return this._http.get(url, httpClient)
            .map((response: Response) => {
                if (response) {
                    if (response.status === 200) {
                        return <any>response.json();
                    }
                }
            })
            .catch((error: any) => {
                return this.throwError(error);
            });
    }

    private throwError(error: any) {
        return Observable.throw(new Error(error.status));
    }

    private handleUnauthorized() {
        return this._router.navigate(['/login', this._router.url]);
    }

    private handleAuthorized() {
        return this._router.navigate(['/home']);
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}