﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using SBase1.Models;

namespace SBase1.Controllers
{
    public partial class TblUser
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
    }
    public class UserController : BaseAPIController
    {
        public HttpResponseMessage Get()
        {
            return ToJson(new TblUser() { Id = 1, FirstName = "f-name", Gender = "M", LastName = "l-name" });
        }

        public HttpResponseMessage Post([FromBody]TblUser value)
        {
            var xxx = value;
            return ToJson(true);
        }

        public HttpResponseMessage Put(int id, [FromBody]TblUser value)
        {
            var xxx = value;
            return ToJson(true);
        }
        public HttpResponseMessage Delete(int id)
        {
            var xxx = id;
            return ToJson(true);
        }
    }
}