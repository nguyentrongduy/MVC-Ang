﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace SBase1.Controllers
{
    public class LoginCheckController : BaseAPIController
    {
        // GET: LoginAPI
        public HttpResponseMessage Get()
        {
            return ToJson(false);
        }
    }

    public class LoginController : BaseAPIController
    {
        // GET: LoginAPI
        public HttpResponseMessage Get()
        {
            return ToJson(false);
        }

        public HttpResponseMessage Post()
        {
            dynamic x = new  { token = "K4L7V7z2beNhmijwiHw98bwO1MvofEiT" };
            return ToJson(x);
        }
    }
}