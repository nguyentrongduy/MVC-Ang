﻿using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Linq;
using System;
using SBase1.Models;
using System.Net.Http.Headers;

namespace SBase1.Controllers
{
    public class BaseAPIController : ApiController
    {
        public readonly EntitiesApplication entities = new EntitiesApplication();

        /// <summary>
        /// Middbleware
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        protected HttpResponseMessage ToJson(dynamic obj)
        {
            var re = Request;
            var headers = re.Headers;
            if(this.CompareVariableWithType(obj, typeof(bool)) && obj == false) {
                if (this.ConditionResponseRequest(headers))
                    return this.ResultResponse(false);
                else
                    return this.ResultResponse(true);
            }
            else
            {
                if (this.ConditionResponseRequest(headers))
                    return this.ResultResponse(obj);
                else
                    return Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
        }

        #region Helper
        private bool ConditionResponseRequest(HttpRequestHeaders headers)
        {
            return (headers.Contains("Authorization") && headers.GetValues("Authorization").First() == this.GetServerToken());
        }

        private HttpResponseMessage ResultResponse(dynamic obj)
        {
            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json");
            return response;
        }

        private string GetServerToken()
        {
            return "K4L7V7z2beNhmijwiHw98bwO1MvofEiT";
        }

        private bool CompareVariableWithType(dynamic obj, Type type)
        {
            return obj.GetType().Equals(type);
        }
        #endregion
    }
}